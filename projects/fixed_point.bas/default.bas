 rem Generated 02/08/2017 9:40:03 by Visual bB Version 1.0.0.566
 rem **********************************
 rem *<filename>                      *
 rem *<description>                   *
 rem *<author>                        *
 rem *<contact info>                  *
 rem *<license>                       *
 rem **********************************
 
 dim _px0 = player0x.a
 dim _mx = c.d

 player0:
 %01000010
 %00100100
 %00011000
 %00011000
 %01011010
 %00101100
 %00011000
 %00011000
end

 playfield:
 XXX.XXXXXX.XX.XX.X.XX.X.XXXXXXXX
 X..............................X
 ...............................X
 ...............................X
 ...............................X
 ...............................X
 ...............................X
 ...............................X
 ...............................X
 ...............................X
 .XXXXXXXXX.XXXX.XX.XX.XX.X.XXXXX
end

 rem player0x = 40
 _px0 = 40.0
 _mx = 0.0
 player0y = 40
 COLUP0 = $30
 COLUBK = $02
 COLUPF = $C6
 scorecolor = $0E

__doloop
 if joy0right then _mx = _mx + 0.1
 if joy0left then _mx = _mx - 0.1

 if _mx > 2 && _mx < 128 then _mx = 2.0
 if _mx > 127 && _mx < 254 then _mx = -2.0

 _px0 = _px0 + _mx

 if _px0 < 8 then _px0 = 8.0: _mx = 0.0
 if _px0 > 150 then _px0 = 150.0: _mx = 0.0
 
 drawscreen 
 goto __doloop