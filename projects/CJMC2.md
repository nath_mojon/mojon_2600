Las pantallas tienen todas la misma configuración porque llevan todas los mismos colores. Dibujar con el editor de Visual BB es un dolor, así que creo que lo haré de alguna otra forma.

Los playfields terminan definiéndose en el código con X y ., y solo se define la mitad izquierda, que luego el HW espeja con el Kernel que estoy usando (multisprite modified).

Los playfield son 16x44, con superladrillos de 8x2 pixels. Pongamos las 11 primeras filas para la copa de los árboles, 22 filas para troncos (terminando en fade out), un gap, y 8 filas inferiores para el "terrain".

Voy a definir, para hacer el motor, un terrain con un agujero en el centro donde luego colocaré una plataforma movil.

El protagonista tendrá salto parabólico, pero no habrá colisión real que comprobar poruqe siempre colisiona con un suelo a la misma altura.

Voy a dibujar en PS y luego convierto de manera tonter.

20170815
========

No sé qué me pasa, con lo diarero que era antes, y ahora apenas escribo nada. Estoy con el movimiento pero no lo tengo fino, quizás porque he hecho algunas paranoias con el orden que suelo seguir, que debe ser:

1.- Aplicar G o Gjumping, dependiendo del salto.
2.- Aplicar VY a Y
3.- Colisionar verticalmente.
4.- Registrar un nuevo salto
5.- Mover horizontalmente

Necesito además un bit para limitar que se deje pulsado "ARRIBA" y otro para el disparo. Necesito además comprobar si estoy "en el suelo" para detener la caída o para poder registrar un nuevo salto.

