 rem Generated 11/08/2017 11:49:53 by Visual bB Version 1.0.0.566
 rem **********************************
 rem *CJMC2                           *
 rem *Spectrum port                   *
 rem *nathanathana                    *
 rem *Calle del Apio, 7               *
 rem *GPL                             *
 rem **********************************

 rem let's try an 8k game and the multisprite kernel

 includesfile multisprite_bankswitch.inc
 set kernel multisprite
 set romsize 8k

 pfheight = 1

 rem ===========================================================================
 rem variable aliases
 rem ===========================================================================
 
 rem Scoreboard
 dim sc1=score
 dim sc2=score+1
 dim sc3=score+2

 rem Player frame
 dim _pl0_frame = a

 rem Player fixed point vars
 dim _pl0_y = player0y.b
 dim _pl0_vy = c.d
 dim _pctj = e
 dim _pl0_x = player0x.g
 dim _pl0_vx = h.i
 dim _ax = j.k
 dim _rx = l.m
 dim _level = n
 dim _enemyai = o

 rem Player stuff
 def _pfacing=f{0}
 def _pjumping=f{1}
 def _pcontroller_up=f{2}
 def _pcontroller_fire=f{3}

 def _g=0.2
 def _gjumping=0.05
 def _vyjumping=3.0

 _level = 1
 
 rem ===========================================================================
 rem Level setup
 rem =========================================================================== 
__setup_level

 on _level goto __set_pf0 __set_pf1
__setup_level_return

 COLUBK=0 
 scorecolor=$1E

 player0x = 20: player0y = 34
 _pl0_vx = 0: _pl0_vy = 0
 _ax = 0.2: _rx = 0.4

 rem ===========================================================================
 rem Main loop
 rem ===========================================================================
__loop

 rem ---------------------------------------------------------------------------
 rem Select player frame

 on _pl0_frame goto __pl0_spr_frame_0 __pl0_spr_frame_1 __pl0_spr_frame_2
__pl0_spr_frame_done

 on _enemyai goto __enai_none __enai_fly
__enai_done

 rem ---------------------------------------------------------------------------
 rem Player movement
 goto __pl0_movement
__pl0_movement_done

 rem ---------------------------------------------------------------------------
 rem Enemy movement

 rem ---------------------------------------------------------------------------
 rem TIA configuration

 NUSIZ0=$15
 COLUP0=$0E

 rem ---------------------------------------------------------------------------
 rem Wait for VSYNC and ride the beam!

 drawscreen

 rem ---------------------------------------------------------------------------
 rem Checks and conditions

 if player0y < 8 then _level = 0: goto __setup_level

 goto __loop

 rem ===========================================================================
 rem Subroutines
 rem ===========================================================================

__pl0_movement
 rem ---------------------------------------------------------------------------
 rem Player movement

 rem Vertical axis
 if _pjumping then _pl0_vy = _pl0_vy - _gjumping else _pl0_vy = _pl0_vy - _g
 _pl0_y = _pl0_y + _pl0_vy

 rem Vertical collision
 rem this game only has downwards collision.
 if _pl0_vy < 128 then goto __pl0_movement_skip_dw_collision

 rem TODO: Add checks for moving platforms
 if player0y >= 34 then goto __pl0_movement_skip_dw_collision
 temp3 = (player0x-11)/4: temp4 = (player0x-4)/4: temp5 = (player0x-8)/4
 if !pfread(temp3,7) goto __pl0_movement_dw_collision_stop
 if !pfread(temp4,7) goto __pl0_movement_dw_collision_stop
 if !pfread(temp5,7) goto __pl0_movement_dw_collision_stop
 goto __pl0_movement_skip_dw_collision
__pl0_movement_dw_collision_stop
 _pl0_y = 34.0
 _pl0_vy = 0.0
__pl0_movement_skip_dw_collision

 rem New jump
 if !joy0up then goto __pl0_movement_end_jump
 if _pcontroller_up then goto __pl0_movement_new_jump_skip
 _pcontroller_up = 1
 if _pjumping then goto __pl0_movement_new_jump_skip
 if player0y > 34 then goto __pl0_movement_new_jump_skip

 _pjumping = 1
 _pctj = 0
 _pl0_vy = _vyjumping
 goto __pl0_movement_new_jump_skip

__pl0_movement_end_jump
 _pcontroller_up = 0
 if !_pjumping then goto __pl0_movement_new_jump_skip 
 _pjumping = 0
 if _pl0_vy > 1 && _pl0_vy < 128 then _pl0_vy = 2.0
 goto __pl0_movement_done
 
__pl0_movement_new_jump_skip
 if !_pjumping then goto __pl0_movement_jump_skip
 _pctj = _pctj + 1: if _pctj = 16 then _pjumping = 0

__pl0_movement_jump_skip

 rem Horizontal axis
 _pl0_frame = 0
 if player0y < 32 then goto __pl0_movement_horz_invalidate
 if joy0left then goto __pl0_movement_left_pressed
 if joy0right then goto __pl0_movement_right_pressed 
__pl0_movement_horz_invalidate

 rem horizontal direction not pressed: deccelerate
 if _pl0_vx < 128 then __pl0_movement_horz_not_pressed_positive
 rem vx negative
 _pl0_vx = _pl0_vx + _rx
 if _pl0_vx < 128 then _pl0_vx = 0.0
 goto __pl0_movement_horizontal_joystick_done
__pl0_movement_horz_not_pressed_positive
 rem vx positive
 _pl0_vx = _pl0_vx - _rx
 if _pl0_vx > 127 then _pl0_vx = 0.0

__pl0_movement_horizontal_joystick_done
 _pl0_x = _pl0_x + _pl0_vx

 if player0x < 15 then _pl0_x = 15.0
 if player0x > 127 then _pl0_x = 127.0

 goto __pl0_movement_done

__pl0_movement_left_pressed 
 _pl0_frame = 1
 _pl0_vx = _pl0_vx - _ax
 rem -2 == 254
 if _pl0_vx > 127 && _pl0_vx < 254 then _pl0_vx = 254.0
 goto __pl0_movement_horizontal_joystick_done

__pl0_movement_right_pressed 
 _pl0_frame = 2
 _pl0_vx = _pl0_vx + _ax
 if _pl0_vx > 2 && _pl0_vx < 128 then _pl0_vx = 2.0
 goto __pl0_movement_horizontal_joystick_done

 rem ---------------------------------------------------------------------------
 rem ENEMY AI ROUTINES

__enai_none 
 goto __enai_done

__enai_fly
 goto __enai_done

 rem ---------------------------------------------------------------------------
 rem set player frame

__pl0_spr_frame_0
 player0:
 %00111100
 %01111110
 %01110010
 %11001111
 %10111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %01111110
 %01111110
 %00100100
 %01011010
 %01011010
 %00100100
end
 goto __pl0_spr_frame_done

__pl0_spr_frame_1
 player0:
 %00111100
 %01111110
 %01111110
 %10001111
 %11110111
 %11110111
 %11111111
 %11111111
 %11111111
 %11111111
 %01111110
 %01111110
 %00011100
 %00101000
 %00111000
 %00010000
end
 goto __pl0_spr_frame_done

__pl0_spr_frame_2
 player0:
 %00111100
 %01111110
 %01111110
 %11110001
 %11101111
 %11101111
 %11111111
 %11111111
 %11111111
 %11111111
 %01111110
 %01111110
 %00111000
 %00010100
 %00011100
 %00001000
end
 goto __pl0_spr_frame_done

 rem ===========================================================================
 rem "Trampolines"
 rem ===========================================================================
__set_pf0
 goto __set_pf0_b2 bank2
__set_pf1
 goto __set_pf1_b2 bank2

 rem ===========================================================================
 rem BANK 2 :: Level backgrounds, kernel code, runtime, etc.
 rem ===========================================================================

 bank 2

 inline multisprite_pfread.asm

__set_pf0_b2
 playfield:
 XX.XXXXX...XXXX.
 XX.XXXXXXXXXXXXX
 XXX.XXXXXXXXXXXX
 XXXXXXXXXXXXXXX.
 XXXX.XXXXXXXXX..
 XXXXX..XXXXX.XXX
 XXXXX.......XXXX
 XXXXX.XXXXX.XXXX
 XXXXX..XXXXX...X
 XXXX....XXXXX...
 XXX......XXXX...
 ................
 .X........X.....
 .X.....X..X.....
 .X.XX..X..XX....
 XX..X..X...XXX..
 X...XX.X.....XX.
 X...XXXX......XX
 X....XXX......XX
 X.....XX...X..XX
 X.....XXXXX...XX
 X.....XX......XX
 X.....XX......XX
 X.X...XX......XX
 X..XXXXX......XX
 X.....XX......XX
 X.....XX......XX
 XX....XXX.....XX
 XX....XXX.....XX
 XX....XXX....XXX
 XX....XXX...XXXX
 XXX...XXX..XX..X
 ................
 ................
 ................
 ................
 XXXX............
 XXXX............
 XXXX............
 XXXX............
 XXXXX...........
 XXXXXX..........
 XXXXXX.........X
 XXXXX.........XX
end
 goto __setup_level_return bank1

__set_pf1_b2
 playfield:
 ..............XX
 XX..........XXXX
 XXX.........XXXX
 XX...XXXXXX..XXX
 ...XXXXXXXXX...X
 ..XXXXXXXXXX....
 ..XXXXXXXXXX..XX
 ....XXXXXXX.XXXX
 XXXXX.......XXXX
 XXXXXX...XXX.XXX
 XXXX...XXXXXX..X
 ......XXXXX.....
 .......X..XX....
 X.....XX...XX...
 XXX.XXX.....XXX.
 ...XX.........XX
 ..XX.......X...X
 ..X.......X.X..X
 .XX..X....X..XXX
 XX...X...XX....X
 XX...XX.XX.....X
 XX....XXX......X
 XXX...XXX......X
 XXX...XXX..X..XX
 XXX...XXXXX...XX
 XXX...XXX.....XX
 XXX...XXX.....XX
 XXX..XXXX.....XX
 .XX..XXXX....XXX
 .XX..XXXX....XXX
 .XX..XXXX....XXX
 .XX..XXXX....XXX
 ................
 ................
 .X......X.X.....
 .X.......X......
 XXX.....XX....XX
 XXX.....XX....XX
 XXX.....XX....XX
 XXX....XXX....XX
 XXX....XXX....XX
 XXX.....XX....XX
 XXX.....XXX...XX
 XX......XX....XX
end
 goto __setup_level_return bank1

 rem ---------------------------------------------------------------------------
 rem Fixed colours for playfield. Must reside in bank 2 so the kernel can find
 rem them without having to resort to bankswitching.
 rem ---------------------------------------------------------------------------

 data msk_bgcolor
 $d0, $d0, $d0, $d2, $d2, $d2, $d2, $d3, $d3, $de, $de, $de, $de
 $f0, $f2, $f4, $f6, $f8, $fa, $fc, $fc, $fc, $fc, $fc, $fc
 $fc, $fc, $fc, $fc, $fa, $f8, $f6, $f4
 $e6, $e6, $e6, $e6, $e4, $e4, $e4, $e2, $e2, $e0, $e0, $e0
 $1e, $2e, $3e, $4e, $5e, $6e, $7e, $8e
end
