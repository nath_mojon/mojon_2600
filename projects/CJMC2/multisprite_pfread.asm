pfread
 clc
 adc #$08
 and #$1F
 cmp #$10
 and #$0F
 tax
 lda bit_masks,x
 bcc skip_PF2_and
 and (PF2pointer),y
 bcs skip_PF1_and
skip_PF2_and
 and (PF1pointer),y
skip_PF1_and
 beq skip_ldFF
 lda #$FF
skip_ldFF
 return

bit_masks
 .byte $01, $02, $04, $08, $10, $20, $40, $80
 .byte $80, $40, $20, $10, $08, $04, $02, $01
