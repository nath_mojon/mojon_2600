 set smartbranching on
 dim rand1=$DA
 set kernel multisprite
 set optimization speed
 set romsize 4k
 const screenheight=88
 ; set debug cycle

 dim gravity=c.d
 dim sc1=score
 dim sc2=score+1
 dim sc3=score+2
 dim SoundPtrLo=p
 dim SoundPtrHi=q
 dim DelayTableLo=s
 dim DelayTableHi=t
 dim _High_Score01=f
 dim _High_Score02=g
 dim _High_Score03=h
 dim _Temp_Score01=i
 dim _Temp_Score02=j
 dim _Temp_Score03=k
 dim _Bit2_Swap_Scores=l
 dim soundindex1=o
 dim soundindex2=p

 dim cityX=z
 dim flag=y
 soundindex1=8: soundindex2=8

startrestart

 COLUPF=14
 flag=0
 gravity=0.13
 e=1
 pfheight=0
 playfield:
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 .......XXX......
 ..X...XXXXX.....
 .XXX.XXXXXXX.XX.
 XXXXXXXXXXXXXXXX
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
 ................
end
 NUSIZ4=$03
cloud1
 player2:
 %01111110
 %00111100
 %00011000
end
 
buildings1
 player4:
 %10110001
 %11111111
 %11111111
 %11111111
 %10110101
 %11111111
 %00010101
 %00011111
end
 COLUP3=30
 COLUP2=12
 COLUP4=8
 goto __Game_Over_Setup

gamebegin
 player5y=98: missile0y=98
 player0y=(rand&40)+92
 player0x=141
 player2x=100
 cityX=165

 player3x=30
 missile1x=24
 player3y=55

 player2y=84

 player4y=17

 player0:
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %01111110
 %11111111
 %11111111
 %11111111
 %11111111
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %00000000
 %11111111
 %11111111
 %11111111
 %11111111
 %01111110
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
 %11111111
end

loop
 
 r=r+1: if r{0} then noread
 player0x=player0x-e
 if player0x<1 || player0x>250 then player0x=141: b=(rand&40)+92: player0y=b: dec sc2=sc2+1
 rem if sc2>$05 then e=2
 rem if sc2>$0A then e=3
noread
 rem sc1=e
 if r{1} && r{0} then cityX=cityX-1
 if cityX=5 then cityX=165 ; Wrap city.
 player4x=cityX
 if r=127 then player2x=player2x-1
 COLUP0=196
 NUSIZ0=$05
 NUSIZ2=$07
 if v=1 then nojoy
 gosub button
nojoy
 gosub nobutton
 rem if joy0fire then temp1=8: temp2=8: gosub setsound

 gosub gravchk
 gosub ds

 missile1y=player3y-2
 if r{0} then noread2
 player0x=player0x-e
 temp6=sc2&$F0
 if player0x<1 || player0x>250 then player0x=141: b=(rand&40)+92: player0y=b: dec sc2=sc2+1
 temp5=sc2&$F0: if temp6<>temp5 then e=e+1
noread2
 if r{1} && r{0} then cityX=cityX-1
 COLUP0=196
 NUSIZ0=$05
 NUSIZ2=$07
 if v=1 then nojoy2
 gosub button
nojoy2
 gosub nobutton
 gosub gravchk
 if player2x<8 then player2x=101 ; Wrap clouds.
 player2x=player2x+64
 rem if r{0} then REFP0=8
 rem if joy0fire then temp1=8: temp2=8: gosub setsound

 gosub ds
 player2x=player2x-64
 if collision(player0,missile1) then gosub Doh: goto startrestart
 rem ** service any sound effects playing. run once per frame
playsoundfx

playsound1
 temp1=soundindex1&%00001111
 if temp1=%00001111 then AUDV0=0: goto playsound2
 AUDF0=sfxdata_f[soundindex1]
 AUDV0=sfxdata_v[soundindex1]
 soundindex1=soundindex1+1

playsound2
 temp1=soundindex2&%00001111
 if temp1=%00001111 then AUDV1=0: goto playsoundfxdone
 AUDF1=sfxdata_f[soundindex2]
 AUDV1=sfxdata_v[soundindex2]
 soundindex2=soundindex2+1
playsoundfxdone

 goto loop

setsound
 temp3=soundindex1&%00001111
 temp4=soundindex2&%00001111
 rem ** if we're going to interrupt a sound, interrupt the one that's closest to finishing
 if temp3>temp4 then soundindex1=temp1: AUDC0=temp2 else soundindex2=temp1: AUDC1=temp2
 return thisbank

Title
__Game_Over_Setup

 rem * Checks for a new high score.
 rem `
 if sc1 > _High_Score01 then goto __New_High_Score
 if sc1 < _High_Score01 then goto __Skip_High_Score

 rem ````````````````````````````````````````````````````````````````
 rem ` First byte equal. Do the next test. 
 rem `
 if sc2 > _High_Score02 then goto __New_High_Score
 if sc2 < _High_Score02 then goto __Skip_High_Score

 rem ````````````````````````````````````````````````````````````````
 rem ` Second byte equal. Do the next test. 
 rem `
 if sc3 > _High_Score03 then goto __New_High_Score
 if sc3 < _High_Score03 then goto __Skip_High_Score

 rem ````````````````````````````````````````````````````````````````
 rem ` All bytes equal. Current score is the same as the high score.
 rem `
 goto __Skip_High_Score

__New_High_Score

 rem ````````````````````````````````````````````````````````````````
 rem ` Save new high score.
 rem `
 _High_Score01=sc1: _High_Score02=sc2: _High_Score03=sc3

__Skip_High_Score

 rem ****************************************************************
 rem *
 rem * Setup for the game over loop.
 rem `
 r=0: v= 1

 _Temp_Score01=sc1: _Temp_Score02=sc2: _Temp_Score03=sc3

Fla
 player0:
 %00110000
 %10100000
 %10101010
 %10101110
 %10101010
 %11000100
 %10000000
 %11110000
end

ppy
 player5:
 %00010000
 %10010000
 %10011001
 %11010101
 %10101001
 %01000010
 %00000000
 %00000000
end

 rem ****************************************************************
 rem ****************************************************************
 rem *
 rem * Game Over Loop
Game_Over

 COLUP5=222: COLUP0=222
 player2y=98: player3y=98: player4y=98
 player0x=67: missile0x=67: missile0y=65 
 player5x=84: missile1x=84: missile1y=66
 player0y=69: player5y=67

 r=r + 1

 rem ````````````````````````````````````````````````````````````````
 rem ` If counter hasn't reached 2 seconds yet, skip this section.
 rem `
 if r < 120 then __Skip_Flip

 rem ````````````````````````````````````````````````````````````````
 rem ` Clear the counter and flip the score swap bit.
 rem `
 r=0: _Bit2_Swap_Scores{2}=!_Bit2_Swap_Scores{2}

 rem ````````````````````````````````````````````````````````````````
 rem ` If the score swap bit is on, display the high score.
 rem `
 if _Bit2_Swap_Scores{2} then scorecolor=$44: sc1=_High_Score01: sc2=_High_Score02: sc3=_High_Score03: goto __Skip_Flip
 rem ````````````````````````````````````````````````````````````````
 rem ` If the score swap bit is off, display the current score.
 rem `
 scorecolor=0: sc1=_Temp_Score01: sc2=_Temp_Score02: sc3=_Temp_Score03 

__Skip_Flip

 gosub ds

 if v=1 then nojoy3
 if joy0fire then v=1: score=0: scorecolor=0: goto gamebegin
nojoy3
 gosub nobutton

 goto Game_Over
 rem goto start

ds
 drawscreen 
 return thisbank

gravchk
 gravity=gravity+0.035
 player3y=player3y-gravity
 if player3y<27 then player3y=27
 if player3y>79 then player3y=79
 return thisbank
button
 if joy0fire then v=1: player3y=player3y+4: gravity=0.7: player3:
 %00100000
 %00110000
 %01111000
 %01111100
 %11111010
 %01110010
 %00000111
 %00000110
end
 temp1=8: temp2=8: gosub setsound
 return thisbank

nobutton
 if !joy0fire then v=0: player3:
 %00000000
 %00000000
 %00110000
 %01111110
 %11111010
 %01110111
 %01100110
 %10000000
end
 return thisbank

 rem ** to use these sound effects, just set temp1=start of 
 rem ** sfx data, temp2=AUDC value, and gosub setsound

 asm
sfxdata_f
 ; flap frequency data. to use... temp1=8: temp2=8: gosub setsound
 BYTE $00, $00, $00, $00, $00, $00, $00, $00
 BYTE $00, $12, $1B, $11, $00, $00, $00, $00

sfxdata_v
 ; shooting volume data...
 BYTE $00, $00, $00, $00, $00, $00, $00, $00
 BYTE $00, $02, $04, $08, $00, $00, $00, $00
end

 data msk_bgcolor
 250, 202, 202, 14, 14, 14, 14, 14, 14, 14, 170, 170, 170, 170, 170
 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170
 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170
 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170
 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170
 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170
 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170
 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170
 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170, 170
end

Doh
 AUDC0=0: AUDC1=0
 inline DOH4.asm
 return thisbank

 ; 
 ; Value	Effect 
 ; $0x	(x=don't care) missile=1 pixel wide 
 ; 	$1x 	missile=2 pixels wide 
 ; 	$2x 	missile=4 pixels wide 
 ; 	$3x 	missile=8 pixels wide 
 ; 	$x0 	one copy of player and missile 
 ; 	$x1 	two close copies of player and missile 
 ; 	$x2 	two medium copies of player and missile 
 ; 	$x3 	three close copies of player and missile 
 ; 	$x4 	two wide copies of player and missile 
 ; 	$x5 	double-sized player 
 ; 	$x6 	three medium copies of player and missile 
 ; 	$x7 	quad-sized player 
