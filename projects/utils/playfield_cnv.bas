#include "file.bi"
#include "fbpng.bi"
#include "fbgfx.bi"
#include once "crt.bi"

#include "cmdlineparser.bi"
#include "mtparser.bi"

#define RGBA_R( c ) ( CUInt( c ) Shr 16 And 255 )
#define RGBA_G( c ) ( CUInt( c ) Shr  8 And 255 )
#define RGBA_B( c ) ( CUInt( c )        And 255 )
#define RGBA_A( c ) ( CUInt( c ) Shr 24         )

Sub usage
	Print "$ playfield_cnv.exe in.png out.txt"
End Sub

Dim As Any Ptr img
Dim As Integer wIn, hIn, x, y
Dim As String liner

If Command (2) = "" Then usage: End

' We need to read the input image at this point
screenres 640, 480, 32, , -1

img = png_load (Command (1))
If ImageInfo (img, wIn, hIn, , , , ) Then
	Puts "There was an error reading input file. Shitty png?": End
End If

If wIn <> 16 And hIn <> 44 Then
	Puts "Wrong dimmensions": End
End If

Open Command (2) For Output As #1

' Upside down
For y = hIn - 1 To 0 Step -1
	liner = " "
	For x = 0 To wIn - 1
		If RGBA_R (Point (x, y, img)) Then
			liner = liner & "X"
		Else
			liner = liner & "."
		End If
	Next x
	Print #1, liner
Next y

Close
