 rem Generated 02/08/2017 9:03:19 by Visual bB Version 1.0.0.566
 rem **********************************
 rem *<filename>                      *
 rem *<description>                   *
 rem *<author>                        *
 rem *<contact info>                  *
 rem *<license>                       *
 rem **********************************

 dim _fix = player0x.b
 dim __ac = c.d
 dim __ac_decimal = d
 dim _ac = e

 rem vamos a usar nuestra propia convenci?n para positivos / negativos
 rem no se me ocurre otra forma, y nadie parece haber usado nada parecido
 rem a tener aceleraci?n en los juegos.

 player0y = 40
 player0x = 40
 rem _fix = 80.0
 COLUBK = 0
 COLUP0 = $30

 c = 0
 _ac = -64


 rem Here's the trick
 rem if acceleration is negative, substract the absolute value
 if e > 127 then __ac_decimal = 255-e+1 else __ac_decimal = e
 rem _fix = _fix + __ac

 player0:
 %00011000
 %00111100
 %01111110
 %01111110
 %01111110
 %01111110
 %00111100
 %00011000
end

__loop

 drawscreen

 goto __loop