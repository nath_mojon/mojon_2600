 rem Generated 01/08/2017 13:11:17 by Visual bB Version 1.0.0.566
 rem **********************************
 rem *Moggerson                       *
 rem *Intento de aprender mierdas     *
 rem *Papana_th_as                    *
 rem *Mojonia es grande y ancha       *
 rem *GPL                             *
 rem **********************************

 rem Versi�n Espa�a
 set tv pal

 rem Configuraci?n del kernel. Con el kernel B?sico se pueden tener 
 rem chiribitas, pero se pierden capacidades. He decidido no usar
 rem misiles. En cambio, mis sprites ser?n de colores:
 set kernel_options player1colors playercolors pfcolors

 rem Queremos un marcador chuli
 const pfscore = 1

 rem Variables (en realidad son alias para acordarse)

 rem Direcci?n. 0 idle, 1 2 3 4 derecha abajo izquierda arriba (CW).
 dim _pl0_direction = a
 dim _pl1_direction = b

 rem Limites de la pantalla
 const _s_edge_top = 9
 const _s_edge_left = 5
 const _s_edge_right = 149
 const _s_edge_bottom = 88

 rem Algunos colores. Si hay que hacer la versi�n NTSC, cambio f�cil
 const _bgcolor = $D0
 const _deathcol = $40
 const _win0color = $28
 const _win1color = $68

 rem Vamos a usar aritm?tica de punto fijo para mover suavemente a los
 rem moggys. bB permite unir dos variables en una sola de punto fijo.
 rem Punto fijo es PARTE_ENTERA.PARTE_DECIMAL. La parte entera se
 rem corresponde con la posici�n real de los sprites, as� que:
 dim _px0 = player0x.c
 dim _py0 = player0y.d
 dim _px1 = player1x.e
 dim _py1 = player1y.f

 rem Las variables de velocidad tambi�n son decimales. La velocidad se
 rem modifica poco a poco al empujar el joystick, as� que punto fijo.
 dim _pmx0 = g.h
 dim _pmy0 = i.j
 dim _pmx1 = k.l
 dim _pmy1 = m.n

 rem contiene el n�mero de bloques obst�culo inicial
 const _max_debris = 5

 rem para los efectos de cambio de color momentaneo
 dim _fx_ctr = p

 rem ball
 dim _ball_height = u 

 rem mejores randoms
 dim rand16 = z

 rem program start

__Start_Restart

 rem sacamos a los sprites de la pantalla
 player0y = 200: player1y = 200

 rem ===========================================================================
 rem PANTALLA DE T�TULO
 rem ===========================================================================
__Title_screen

 pfcolors:
 $00
 $30
 $00
 $32
 $34
 $36
 $38
 $3A
 $3B
 $30
 $00
end
 playfield:
 ................................
 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 ................................
 .X...X..XX...XX...XX..XXX.XXX...
 .XX.XX.X..X.X....X....X...X..X..
 .X.X.X.X..X.X.X..X.X..XX..XXX...
 .X...X.X..X.X..X.X..X.X...X.X...
 .X...X..XX...XX...XX..XXX.X..X..
 ................................
 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
 ................................
end

 rem Color de fondo
 COLUBK = _bgcolor

 rem ===========================================================================
__Title_Screen_loop
 drawscreen
 if !joy0fire && !joy1fire then goto __Title_Screen_loop
__Debounce
 drawscreen
 if joy0fire || joy1fire then goto __Debounce

 rem 
 rem ===========================================================================
__Game_setup

 rem 
 rem ===========================================================================
__Level_setup

 rem Empezamos a prepara el juego. Aqu? preparamos la pantalla
 rem e inicializamos las puntuaciones y las pollas.

 pfscorecolor = $6E
 scorecolor = $6E
 
 rem Esta es una forma como cualquier otra de borrar el playfield
 pfclear

 rem Ya que puedo, pongo colores en las lineas del fondo
 pfcolors:
 $30
 $30
 $32
 $32
 $34
 $36
 $34
 $32
 $32
 $30
 $30
end

 rem Los colores de los sprites de los jugadores no cambiar?n, as? que los 
 rem podemos poner aqu?.
 player0color:
 $22
 $24
 $26
 $28
 $28
 $26
 $24
 $22
end
 player1color:
 $62
 $64
 $66
 $68
 $68
 $66
 $64
 $62
end

 rem ponemos a los jugadores en su posici?n inicial.
 _px0 =  36.0: _py0 = 40.0
 _px1 = 116.0: _py1 = 40.0

 rem movimiento a 0.
 _pmx0 = 0.0: _pmy0 = 0.0
 _pmx1 = 0.0: _pmy1 = 0.0

 rem direcciones
 _pl0_direction = 0
 _pl1_direction = 0

 rem pfscore se utiliza como barra de vida.
 pfscore1 = %11111111
 pfscore2 = %11111111

 rem rellenamos al azar algunos bloques en el fondo
 for q = 0 to _max_debris
  gosub __put_new_debris
 next q

 rem definimos el aspecto de "ball", que usaremos como item.
 rem "2" significa "4 pixels de ancho"
 CTRLPF = $21
 _ball_height = 1
 gosub _throw_ball

 rem ===========================================================================
 rem BUCLE PRINCIPAL
 rem ===========================================================================
  
__Game_loop
 rem guardamos en variables temporales las posiciones actuales.
 temp3 = player0x
 temp4 = player0y
 temp5 = player1x
 temp6 = player1y
 
 rem Vamos a seleccionar el frame para el player 0
 on _pl0_direction goto __pl0d0 __pl0d1 __pl0d2 __pl0d3 __pl0d4
__pl0d_done
 rem Vamos a seleccionar el frame para el player 1.
 on _pl1_direction goto __pl1d0 __pl1d1 __pl1d2 __pl1d3 __pl1d4
__pl1d_done

 rem Movimiento de los jugadores
 goto __pl0_movement
__pl0_movement_done
 goto __pl1_movement
__pl1_movement_done

 rem Colisi�n con el fondo.
 goto __pl0_bg_collision
__pl0_bg_collision_done
 goto __pl1_bg_collision
__pl1_bg_collision_done

 rem Colisi�n entre los jugadores.
 goto __pl0_pl1_collision
__pl0_pl_bg_collision_done

 rem colisi�n con la bola
 goto __pl0_ball_collision
__pl0_ball_collision_done
 goto __pl1_ball_collision
__pl1_ball_collision_done

 rem efectos de cambio de color. Por ahi se cambiar� el color del fondo y 
 rem se pondr� un valor en el contador _fx_ctr . Aqu� lo desactivamos.
 if _fx_ctr = 0 then goto __skip_fx_ctr
 _fx_ctr = _fx_ctr - 1
 if _fx_ctr = 0 then COLUBK = _bgcolor
__skip_fx_ctr

 rem intentamos que la bola se vea un poco m�s haciendo que cambie su
 rem tama�o continuamente (barato)
 ballheight = _ball_height
 _ball_height = _ball_height + 1: if _ball_height = 4 then _ball_height = 0

 rem Se pinta la pantalla
 drawscreen

 rem condici�n de salida
 if pfscore1 = 0 || pfscore2 = 0 then goto __Game_over
 goto __Game_loop

 rem ===========================================================================
 rem GAME OVER
 rem ===========================================================================

__Game_over
 goto __Start_Restart

 rem ===========================================================================
 rem SUBRUTINAS
 rem ===========================================================================

 rem ===========================================================================
 rem Movimiento para el jugador 0. 
__pl0_movement
 rem Primero leemos los mandos
 if joy0up    then goto __pl0_joyup
 if joy0down  then goto __pl0_joydown
__pl0_joyvert_done
 if joy0left  then goto __pl0_joyleft
 if joy0right then goto __pl0_joyright
__pl0_joyhorz_done

 rem Luego aplicamos el movimiento
 _px0 = _px0 + _pmx0
 _py0 = _py0 + _pmy0
 
 rem Y comprobamos los bordes. 
 rem Los && extras horizontales son por la mierda los negativos.
 if _px0 < _s_edge_left || _px0 > 170 then _px0 = _s_edge_left: _pmx0 = 0.0
 if _px0 > _s_edge_right && _px0 < 165 then _px0 = _s_edge_right: _pmx0 = 0.0
 if _py0 < _s_edge_top then _py0 = _s_edge_top: _pmy0 = 0.0
 if _py0 > _s_edge_bottom then _py0 = _s_edge_bottom: _pmy0 = 0.0

 goto __pl0_movement_done

 rem Esto es un poco confuso. Recordemos que no tenemos negativos de forma 
 rem expl?cita. En su lugar, de 128..255 equivale a -128..-1, en ese orden.

__pl0_joyup
 _pmy0 = _pmy0 - 0.1
 if _pmy0 > 127 && _pmy0 < 254 then _pmy0 = -2.0
 goto __pl0_joyvert_done 
 
__pl0_joydown
 _pmy0 = _pmy0 + 0.1
 if _pmy0 < 128 && _pmy0 > 2 then _pmy0 = 2.0
 goto __pl0_joyvert_done
 
__pl0_joyleft
 _pmx0 = _pmx0 - 0.1
 if _pmx0 > 127 && _pmx0 < 254 then _pmx0 = -2.0
 goto __pl0_joyhorz_done
 
__pl0_joyright
 _pmx0 = _pmx0 + 0.1
 if _pmx0 < 128 && _pmx0 > 2 then _pmx0 = 2.0
 goto __pl0_joyhorz_done
 
 rem Movimiento para el jugador 1. Recuerden, no hay negativos. -1 equivale a 255
__pl1_movement
 rem Primero leemos los mandos
 if joy1up    then goto __pl1_joyup
 if joy1down  then goto __pl1_joydown
__pl1_joyvert_done
 if joy1left  then goto __pl1_joyleft
 if joy1right then goto __pl1_joyright
__pl1_joyhorz_done

 rem Luego aplicamos el movimiento
 _px1 = _px1 + _pmx1
 _py1 = _py1 + _pmy1
 
 rem Y comprobamos los bordes. 
 rem Los && extras horizontales son por la mierda los negativos.
 if _px1 < _s_edge_left || _px1 > 170 then _px1 = _s_edge_left: _pmx1 = 0.0
 if _px1 > _s_edge_right && _px1 < 165 then _px1 = _s_edge_right: _pmx1 = 0.0
 if _py1 < _s_edge_top then _py1 = _s_edge_top: _pmy1 = 0.0
 if _py1 > _s_edge_bottom then _py1 = _s_edge_bottom: _pmy1 = 0.0

 goto __pl1_movement_done

 rem Esto es un poco confuso. Recordemos que no tenemos negativos de forma 
 rem expl?cita. En su lugar, de 128..255 equivale a -128..-1, en ese orden.

 rem -0.64 :: 
__pl1_joyup
 _pmy1 = _pmy1 - 0.1
 if _pmy1 > 127 && _pmy1 < 254 then _pmy1 = -2.0
 goto __pl1_joyvert_done 
 
__pl1_joydown
 _pmy1 = _pmy1 + 0.1
 if _pmy1 < 128 && _pmy1 > 2 then _pmy1 = 2.0
 goto __pl1_joyvert_done
 
__pl1_joyleft
 _pmx1 = _pmx1 - 0.1
 if _pmx1 > 127 && _pmx1 < 254 then _pmx1 = -2.0
 goto __pl1_joyhorz_done
 
__pl1_joyright
 _pmx1 = _pmx1 + 0.1
 if _pmx1 < 128 && _pmx1 > 2 then _pmx1 = 2.0
 goto __pl1_joyhorz_done


 rem ===========================================================================
 rem Coloca un nuevo bloque con cuidado de que no pise a nadie
__put_new_debris
 x = rand&31
___newy2
 y = rand&15: if y > 10 then goto ___newy2

 rem No esta encima de nadie, verdad?
 temp1 = (player0x-16+4)/4
 temp2 = (player0y-8+4)/8
 if x = temp1 && y = temp2 then goto __put_new_debris
 temp1 = (player1x-16+4)/4
 temp2 = (player1y-8+4)/8
 if x = temp1 && y = temp2 then goto __put_new_debris

 rem Colocamos
 pfpixel x y set

 return

 rem ===========================================================================
 rem Coloca la pelota por ah�
_throw_ball
 rem Siempre que se coloca la pelota, aparece m�s basura en el fondow.
 gosub __put_new_debris

 ballx = rand
 if ballx < _s_edge_left+4 then goto _throw_ball
 if ballx+4 > _s_edge_right then goto _throw_ball
_throw_ball_y
 bally = rand&127
 if bally < _s_edge_top+4 then goto _throw_ball_y
 if bally+4 > _s_edge_bottom then goto _throw_ball_y
 return

  rem ===========================================================================
  rem Colisi�n con el fondo
__pl0_bg_collision
 x = (player0x-16+4)/4
 y = (player0y-8+4)/8
 if x > 31 then goto __pl0_bg_collision_done
 if !pfread (x,y) then goto __pl0_bg_collision_done
 pfpixel x y off
 gosub __put_new_debris
 COLUBK = _deathcol
 _fx_ctr = 8

 rem Tengo que quitar el bit que sea, contando desde la izquierda.
 rem Si nos fijamos equivale a shift right: 00011111 -> 00001111
 rem Y shift right equivale a dividir por 2:
 pfscore1 = pfscore1 / 2

 rem a�adimos puntos al otro jugador
 score = score + 1
 goto __pl0_bg_collision_done

__pl1_bg_collision
 x = (player1x-16+4)/4
 y = (player1y-8+4)/8
 if x > 31 then goto __pl1_bg_collision_done
 if !pfread (x,y) then goto __pl1_bg_collision_done
 pfpixel x y off
 gosub __put_new_debris
 COLUBK = _deathcol
 _fx_ctr = 8

 rem Tengo que quitar el bit que sea, contando desde la derecha.
 rem Si n os fijamos equivale a un shift left: 11111000 -> 11110000
 rem Y shift left equivale a multiplicar por 2:
 pfscore2 = pfscore2 * 2 

 rem a�adimos puntos al otro jugador
 score = score + 10000
 goto __pl1_bg_collision_done

 rem ===========================================================================
 rem Colisi�n entre jugadores

__pl0_pl1_collision
 rem Lo hacemos por caja de to la vida.
 if player0x + 7 < player1x then goto __pl0_pl_bg_collision_done
 if player0x > player1x + 7 then goto __pl0_pl_bg_collision_done
 if player0y + 7 < player1y then goto __pl0_pl_bg_collision_done
 if player0y > player1y + 7 then goto __pl0_pl_bg_collision_done
 COLUBK = _deathcol
 _fx_ctr = 8 
 rem En una colisi�n as�, se intercambian las velocidades.
 rem mx0 g.h mx1 k.l
 temp1 = g: temp2 = h: g = k: h = l: k = temp1: l = temp2
 rem my0 i.j my1 m.n
 temp1 = i: temp2 = j: i = m: j = n: m = temp1: n = temp2
 rem Reposicionamos
 player0x = temp3
 player0y = temp4
 player1x = temp5
 player1y = temp6
 goto __pl0_pl_bg_collision_done
 
 rem ===========================================================================
 rem Colisi�n con la bola

 rem Notas sobre la bola: aparece donde le sale del co�o. el sprite (que en este
 rem juego es un cuadrado de 4x4 p�xels) aparecer� exactamente un pixel a la 
 rem izquierda y cinco m�s abajo de lo que digan ballx y bally. Ahora hay que
 rem hacer algo de fuller�a para detectar la colisi�n.

 rem me he hecho dibujos en papel de cuadritos y sale esto:
 
 rem En X, habr� colisi�n si ballx >= player0x - 2 y ballx <= player0x + 8
 rem en Y, habr� colisi�n si bally >= player0y - 8 y bally <= player0y + 2

 rem me gusta coger las inecuaciones y mover factores para que no haya restas
 rem aprend� esto hace tiempo, evita problemas en los bordes porque nunca jam�s
 rem los valores se van a negativo!

 rem los 4 ifs "descartan", por eso las inecuaciones son las contrarias.

__pl0_ball_collision
 if ballx + 2 < player0x then goto __pl0_ball_collision_done
 if ballx > player0x + 8 then goto __pl0_ball_collision_done
 if bally + 8 < player0y then goto __pl0_ball_collision_done
 if bally > player0y + 2 then goto __pl0_ball_collision_done
 COLUBK = _win0color
 _fx_ctr = 8
 score = score + 10000
 gosub _throw_ball
 goto __pl0_ball_collision_done

__pl1_ball_collision
 if ballx + 2 < player1x then goto __pl1_ball_collision_done
 if ballx > player1x + 8 then goto __pl1_ball_collision_done
 if bally + 8 < player1y then goto __pl1_ball_collision_done
 if bally > player1y + 2 then goto __pl1_ball_collision_done
 COLUBK = _win1color
 _fx_ctr = 8
 score = score + 1
 gosub _throw_ball
 goto __pl1_ball_collision_done

 rem ===========================================================================
 rem Selecci?n de la direcci?n para los jugadores
__pl0d0
 player0:
 %00111100
 %01111110
 %11100111
 %11111111
 %10111101
 %11111111
 %01111110
 %00111100
end
 goto __pl0d_done

__pl0d1
 player0:
 %00111100
 %01111110
 %11110000
 %11111111
 %11101111
 %11111111
 %01111110
 %00111100
end
 goto __pl0d_done

__pl0d2
 player0:
 %00011100
 %01011110
 %11011111
 %11011111
 %11110111
 %11111111
 %01111110
 %00111100
end
 goto __pl0d_done

__pl0d3
 player0:
 %00111100
 %01111110
 %11111111
 %11110111
 %11111111
 %00001111
 %01111110
 %00111100
end
 goto __pl0d_done

__pl0d4
 player0:
 %00111100
 %01111110
 %11111111
 %11101111
 %11111011
 %11111011
 %01111010
 %00111000
end
 goto __pl0d_done

__pl1d0
 player1:
 %00111100
 %01111110
 %11100111
 %11100111
 %11111111
 %11011011
 %01111110
 %00111100
end
 goto __pl1d_done

__pl1d1
 player1:
 %00111110
 %01111000
 %11111000
 %11111111
 %11111111
 %11110111
 %01111110
 %00111100
end
 goto __pl1d_done

__pl1d2
 player1:
 %00011100
 %10011110
 %10011111
 %11111011
 %11111111
 %11111111
 %01111110
 %00111100
end
 goto __pl1d_done

__pl1d3
 player1:
 %00111100
 %01111110
 %11101111
 %11111111
 %11111111
 %00011111
 %00011110
 %01111100
end
 goto __pl1d_done

__pl1d4
 player1:
 %00111100
 %01111110
 %11101111
 %11111111
 %11111111
 %00011111
 %00011110
 %01111100
end
 goto __pl1d_done


